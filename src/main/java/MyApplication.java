import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

public class MyApplication {
    ArrayList<User> users;
    ArrayList<Friends> friends;
    LinkedList<Chat> chats = new LinkedList<>();
    private Scanner sc = new Scanner(System.in);
    private User signedUser;

    public MyApplication() {
        users = new ArrayList<>();
    }

    private void addUser(User user) {
        users.add(user);
    }

    private void menu() throws IOException {
        while (true) {
            if (signedUser == null) {
                System.out.println("You are not signed in.");
                System.out.println("1. Authentication");
                System.out.println("2. Exit");
                int choice = sc.nextInt();
                if (choice == 1) authentication();
                else return;
            } else {
                userProfile();
            }
        }
    }

    public void chatPage() {

    }

    private void userProfile() {
        signedUser = null;
    }

    private void logOff() throws IOException {
        saveUserList();
        menu();
    }

    private void authentication() throws IOException {
        while (true) {
            System.out.println("1. Log in");
            System.out.println("2. Sign up");
            System.out.println("3. Exit");
            int choice = sc.nextInt();
            if (choice == 1) logIn();
            else if (choice == 2) signUp();
            else if (choice == 3) return;
        }
    }

    private void logIn() throws IOException {
        while (true) {
            System.out.println("Enter your username: ");
            String username = sc.next();
            System.out.println("Enter your password: ");
            String passwordStr = sc.next();
            Password password = new Password(passwordStr);
            if (checkUser(username, password)) {
                menuList();
            } else {
                System.out.println("Invalid username or password");
            }
        }
    }

    private void signUp() {
        outer:
        while (true) {
            System.out.println("Enter your name: ");
            String name = sc.next();
            System.out.println("Enter your username: ");
            String username = sc.next();
            System.out.println("Enter your surname: ");
            String surname = sc.next();
            if (checkUsername(username)) {
                System.out.println("Enter your password: ");
                String passwordStr = sc.next();
                Password password = new Password(passwordStr);
                signedUser = new User(name, surname, username, password);
                addUser(signedUser);
            } else {
                System.out.println("This username has already signed");
            }

        }
    }

    public void menuList() throws IOException {
        while (true) {
            System.out.println("1. Friends");
            System.out.println("2. Log off");
            System.out.println("3. My chats");
            int choice = sc.nextInt();
            if (choice == 1) {
                friendsMenu();

            } else if (choice == 2) {
                logOff();
            }
        }
    }


    public void addFriendMenu() {
        while (true) {
            System.out.println("Enter name and surname: ");
            String name = sc.next();
            String surname = sc.next();
            if (checkNewFriend(name, surname)) {
                System.out.println("Request");
                System.out.println("1. Back");
                int choice = sc.nextInt();
                if (choice == 1) friendsMenu();
            }
        }
    }


    public void friendsList() {
        for (Friends f : friends) {
            System.out.println(f);
        }
    }

    public void start() throws IOException {
        readFile();

        while (true) {
            System.out.println("Welcome to my application");
            System.out.println("Select command:");
            System.out.println("1. Menu");
            System.out.println("2. Exit");
            int choice = sc.nextInt();
            if (choice == 1) {
                menu();
            } else {
                break;
            }
        }

    }

    private void readFile() throws FileNotFoundException {
        File file = new File("C:\\Users\\Админ\\Desktop\\users.txt");
        Scanner fileScanner = new Scanner(file);
        while (fileScanner.hasNextLine()) {
            String line = fileScanner.nextLine();
            String[] data = line.split(" ");
            Password password = new Password(data[4]);
            User clone = new User(Integer.parseInt(data[0]), data[1], data[2], data[3], password);
            users.add(clone);
        }
    }

    private boolean checkUsername(String usernameStr) {
        for (User user : users) {
            if (user.getUsername().equals(usernameStr)) {
                return false;
            }
        }
        return true;
    }

    private boolean checkNewFriend(String name, String surname) {
        for (User user : users) {
            if (user.getName().equals(name) && user.getSurname().equals(surname))
                return true;
        }
        return false;
    }

    private boolean checkUser(String username, Password password) {
        for (User user : users) {
            if (user.getUsername().equals(username) && user.getPassword() == password) {
                return true;
            }
        }
        return false;
    }

    
}
