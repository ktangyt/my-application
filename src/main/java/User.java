import java.io.File;
import java.util.Scanner;


public class User {
    private static int id_gen = 0;
    private int id;
    private String name;
    private String surname;
    private String username;
    private Password password;

    public User() {
        idGen();
    }

    public User(String username, Password password) {
        this();
        setUsername(username);
        setPassword(password);
    }

    public User(String name, String surname, String username, Password password) {
        this();
        setName(name);
        setSurname(surname);
        setUsername(username);
        setPassword(password);
    }

    protected User(int id, String name, String surname, String username, Password password) {
        this.id = id;
        setName(name);
        setSurname(surname);
        setUsername(username);
        setPassword(password);
    }

    public static void updateIdGen(int id) {
        if (id_gen < id + 1) {
            id_gen = id + 1;
        }
    }

    private void readId() {
        try {
            File file = new File("C:\\Users\\Админ\\Desktop\\users.txt");
            Scanner sc = new Scanner(file);
            while (sc.hasNextLine()) {
                id_gen = sc.nextInt();
            }
        } catch (Exception e) {
            System.out.println("User.readId : File not found" + e);
        }
        ++id_gen;
    }

    private void idGen() {
        readId();
        id = id_gen++;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Password getPassword() {
        return password;
    }

    public void setPassword(Password password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + surname + " " + username + " " + password;
    }
}
